package com.rivaldy.dmp.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmailAlreadyExistsException extends RuntimeException{
    private final String message;

    public EmailAlreadyExistsException(String message, Throwable cause, boolean enableSuppresion, boolean writableStackTrace){
        super(message, cause, enableSuppresion, writableStackTrace);
        this.message = message;
    }
}

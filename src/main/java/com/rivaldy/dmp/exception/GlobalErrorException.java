package com.rivaldy.dmp.exception;

import com.rivaldy.dmp.enumerate.MessageError;
import com.rivaldy.dmp.pojo.response.ExceptionResponse;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalErrorException extends ResponseEntityExceptionHandler {

    private List<String> errorDescriptions(BindingResult result){
        return result.getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ExceptionResponse(
                        String.valueOf(HttpStatus.BAD_REQUEST.value()),
                        MessageError.DATA_BAD_REQUEST.getMessage(),
                        errorDescriptions(ex.getBindingResult())
                ));
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponse> handleUsernameAlreadyExists(EmailAlreadyExistsException ex){
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(new ExceptionResponse(
                        String.valueOf(HttpStatus.CONFLICT.value()),
                        String.format(MessageError.USER_EMAIL_ALREADY_EXISTS.getMessage(), ex.getMessage())
                ));
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponse> handleUserNotFound(UserNotFoundException ex){
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ExceptionResponse(
                        String.valueOf(HttpStatus.NOT_FOUND.value()),
                        String.format(MessageError.USER_NOT_FOUND.getMessage(), ex.getMessage())
                ));
    }

    @ExceptionHandler(JobNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponse> handleJobNotFound(JobNotFoundException ex){
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ExceptionResponse(
                        String.valueOf(HttpStatus.NOT_FOUND.value()),
                        String.format(MessageError.JOB_NOT_FOUND.getMessage(), ex.getMessage())
                ));
    }

    @ExceptionHandler(UserPasswordNotMatchException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponse> handleJobNotFound(UserPasswordNotMatchException ex){
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(new ExceptionResponse(
                        String.valueOf(HttpStatus.CONFLICT.value()),
                        String.format(MessageError.PASSWORD_MISMATCH.getMessage(), ex.getMessage())
                ));
    }
}

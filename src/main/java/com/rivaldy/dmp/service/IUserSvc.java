package com.rivaldy.dmp.service;

import com.rivaldy.dmp.entity.User;
import com.rivaldy.dmp.pojo.request.LoginUserRequest;
import com.rivaldy.dmp.pojo.request.RegistrationUserRequest;
import com.rivaldy.dmp.pojo.response.AuthUserResponse;

import java.util.List;

public interface IUserSvc {

    AuthUserResponse registrationUser(RegistrationUserRequest request);
    User findByUsername(String username);

    AuthUserResponse loginUser(LoginUserRequest request);
    List<User> findAllUsers();
}

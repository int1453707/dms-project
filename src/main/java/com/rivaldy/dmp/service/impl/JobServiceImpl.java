package com.rivaldy.dmp.service.impl;

import com.rivaldy.dmp.exception.JobNotFoundException;
import com.rivaldy.dmp.pojo.response.RecruitmentJobResponse;
import com.rivaldy.dmp.service.IJobSvc;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class JobServiceImpl implements IJobSvc {

    private final RestTemplate restTemplate;
    private final String url = "http://dev3.dansmultipro.co.id/api/recruitment/positions";

    @Override
    public List<RecruitmentJobResponse> getListOfJobs() {
        return Arrays.stream(restTemplate.getForObject(url+".json", RecruitmentJobResponse[].class))
                .collect(Collectors.toList());
    }

    @Override
    public RecruitmentJobResponse getJobById(String jobId) {
        RecruitmentJobResponse job = restTemplate.getForObject(url+"/"+jobId, RecruitmentJobResponse.class);
        if(job == null){
            throw new JobNotFoundException(jobId);
        }
        return job;
    }
}

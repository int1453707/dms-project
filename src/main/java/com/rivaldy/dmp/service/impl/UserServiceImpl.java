package com.rivaldy.dmp.service.impl;

import com.rivaldy.dmp.config.JwtService;
import com.rivaldy.dmp.entity.User;
import com.rivaldy.dmp.enumerate.RoleTypeEnum;
import com.rivaldy.dmp.enumerate.StatusTypeEnum;
import com.rivaldy.dmp.exception.EmailAlreadyExistsException;
import com.rivaldy.dmp.exception.UserNotFoundException;
import com.rivaldy.dmp.exception.UserPasswordNotMatchException;
import com.rivaldy.dmp.pojo.request.LoginUserRequest;
import com.rivaldy.dmp.pojo.request.RegistrationUserRequest;
import com.rivaldy.dmp.pojo.response.AuthUserResponse;
import com.rivaldy.dmp.repository.UserRepository;
import com.rivaldy.dmp.service.IUserSvc;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements IUserSvc {

    private final UserRepository repository;
    private final JwtService jwtService;

    private Boolean checkExistsUserEmail(String email) {
        return repository.checkExistsEmailUser(email);
    }

    @Override
    public AuthUserResponse registrationUser(RegistrationUserRequest request) {
        if(checkExistsUserEmail(request.getEmail())){
            throw new EmailAlreadyExistsException(request.getEmail());
        }

        User user = User.builder()
                .password(request.getPassword())
                .username(request.getEmail())
                .createdAt(LocalDateTime.now())
                .role(request.getRole()!=null ? request.getRole():RoleTypeEnum.CUSTOMER)
                .status(StatusTypeEnum.ACTIVE)
                .build();

        repository.save(user);

        String jwtToken = jwtService.generateToken(user);
        return new AuthUserResponse(jwtToken);
    }

    @Override
    public User findByUsername(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
    }

    @Override
    public AuthUserResponse loginUser(LoginUserRequest request) {
        if(!checkExistsUserEmail(request.getEmail())){
            throw new UserNotFoundException(request.getEmail());
        }

        User user = findByUsername(request.getEmail());
        if(!user.getPassword().equals(request.getPassword())){
            throw new UserPasswordNotMatchException("");
        }

        String jwtToken = jwtService.generateToken(user);
        return new AuthUserResponse(jwtToken);
    }

    @Override
    public List<User> findAllUsers() {
        return repository.findAll();
    }
}

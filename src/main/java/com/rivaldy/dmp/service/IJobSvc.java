package com.rivaldy.dmp.service;

import com.rivaldy.dmp.pojo.response.RecruitmentJobResponse;

import java.util.List;

public interface IJobSvc {

    List<RecruitmentJobResponse> getListOfJobs();
    RecruitmentJobResponse getJobById(String jobId);
}

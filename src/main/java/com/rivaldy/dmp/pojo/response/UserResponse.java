package com.rivaldy.dmp.pojo.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rivaldy.dmp.entity.User;
import com.rivaldy.dmp.util.FormatDateValue;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse {

    private String username;
    private String email;
    private String createdAt;
    private String updatedAt;
    private String role;
    private String status;

    public UserResponse(User user) {
        this.username = user.getUsername();
        this.createdAt = new FormatDateValue().convertLocalDateToString(user.getCreatedAt());
        this.updatedAt = new FormatDateValue().convertLocalDateToString(user.getUpdatedAt());
        this.role = user.getRole().name();
        this.status = user.getStatus().getMessage();
    }
}

package com.rivaldy.dmp.pojo.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RecruitmentJobResponse {

    private String id;
    private String type;
    private String url;
    @JsonProperty("created_at")
    private String createdAt;
    private String company;
    private String location;
    private String title;
    private String description;
}

package com.rivaldy.dmp.pojo.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class ExceptionResponse {

    private String code;
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object errorReason;

    public ExceptionResponse(String code, String message, Object errorReason){
        this.code = code;
        this.message = message;
        this.errorReason = errorReason;
    }

    public ExceptionResponse(String code, String message){
        this.code = code;
        this.message = message;
    }
}

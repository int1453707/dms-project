package com.rivaldy.dmp.pojo.response;

import lombok.Data;

@Data
public class AuthUserResponse {
    private String token;

    public AuthUserResponse(String token) {
        this.token = token;
    }
}

package com.rivaldy.dmp.pojo.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class LoginUserRequest {

    @Email
    @NotEmpty(message = "Email must not be empty")
    @Size(max = 50, message = "Email characters max 50")
    private String email;
    @NotEmpty(message = "Password must not be empty")
    @Size(min = 8, max = 30, message = "Password must be at least 8 characters and maximum 30 characters")
    private String password;
}

package com.rivaldy.dmp.controller;

import com.rivaldy.dmp.enumerate.MessageEnum;
import com.rivaldy.dmp.pojo.response.ApiResponse;
import com.rivaldy.dmp.service.IJobSvc;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/job")
@AllArgsConstructor
public class JobController {

    private final IJobSvc jobService;

    @GetMapping
    public ResponseEntity<ApiResponse> getAllJobs(){
        return ResponseEntity.ok(
                new ApiResponse(String.valueOf(HttpStatus.OK.value()),
                        MessageEnum.SUCCESS_GET_DATA.getMessage(),
                        jobService.getListOfJobs())
        );
    }

    @GetMapping("/get/{jobId}")
    public ResponseEntity<ApiResponse> getJobs(@PathVariable("jobId") String jobId){
        return ResponseEntity.ok(
                new ApiResponse(String.valueOf(HttpStatus.OK.value()),
                        MessageEnum.SUCCESS_GET_DATA.getMessage(),
                        jobService.getJobById(jobId))
        );
    }
}

package com.rivaldy.dmp.controller;

import com.rivaldy.dmp.enumerate.MessageEnum;
import com.rivaldy.dmp.pojo.request.LoginUserRequest;
import com.rivaldy.dmp.pojo.request.RegistrationUserRequest;
import com.rivaldy.dmp.pojo.response.ApiResponse;
import com.rivaldy.dmp.pojo.response.UserResponse;
import com.rivaldy.dmp.service.IUserSvc;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserController {

    private final IUserSvc userService;
    @PostMapping("/register")
    public ResponseEntity<ApiResponse> registerUser(@Valid @RequestBody RegistrationUserRequest request){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new ApiResponse(
                        String.valueOf(HttpStatus.CREATED.value()),
                        MessageEnum.REGISTER_SUCCESS.getMessage(),
                        userService.registrationUser(request)
                ));
    }

    @PostMapping("/login")
    public ResponseEntity loginUser(@Valid @RequestBody LoginUserRequest request){
        return ResponseEntity
                .ok(new ApiResponse(
                        String.valueOf(HttpStatus.OK.value()),
                        MessageEnum.LOGIN_SUCCESS.getMessage(),
                        userService.loginUser(request))
                );
    }

    @GetMapping("/list")
    public ResponseEntity getListUsers(){
        return ResponseEntity.ok(new ApiResponse(
                String.valueOf(HttpStatus.OK.value()),
                MessageEnum.SUCCESS_GET_DATA.getMessage(),
                userService.findAllUsers().stream()
                        .map(UserResponse::new).collect(Collectors.toList()))
        );
    }

    @GetMapping("/get/{id}")
    public void getUser(@PathVariable("id") String id){

    }
}

package com.rivaldy.dmp.enumerate;

public enum MessageError {

    DATA_BAD_REQUEST("Request Data is Invalid!"),
    USER_EMAIL_ALREADY_EXISTS("User with Email %s is already exists"),
    USER_NOT_FOUND("Email user %s is not found!"),
    JOB_NOT_FOUND("Job with id %s is not found!"),
    PASSWORD_MISMATCH("Password is not match, please try again!");

    private final String message;
    MessageError(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}

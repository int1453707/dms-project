package com.rivaldy.dmp.enumerate;

public enum MessageEnum {

    SUCCESS_GET_DATA("Success Get Data"),
    FAIL_GET_DATA("Failure to Get Data"),
    REGISTER_SUCCESS("Registration User Success"),
    LOGIN_SUCCESS("Login Success");

    private final String message;

    MessageEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}

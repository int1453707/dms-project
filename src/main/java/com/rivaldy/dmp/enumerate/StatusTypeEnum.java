package com.rivaldy.dmp.enumerate;

public enum StatusTypeEnum {
    ACTIVE("Active"),
    INACTIVE("In Active");

    private final String message;

    StatusTypeEnum(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }
}

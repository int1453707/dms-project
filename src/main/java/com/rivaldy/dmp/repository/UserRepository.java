package com.rivaldy.dmp.repository;

import com.rivaldy.dmp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByUsername(String email);

    @Query("SELECT CASE WHEN COUNT(s) > 0 " +
            "THEN TRUE ELSE FALSE END " +
            "FROM User s " +
            "WHERE s.username = :username")
    Boolean checkExistsEmailUser(String username);

    @Query("SELECT CASE WHEN COUNT(s) > 0 " +
            "THEN TRUE ELSE FALSE END " +
            "FROM User s " +
            "WHERE s.username = :username " +
            "AND s.password = :password ")
    Boolean checkUserEmailAndPassword(String username, String password);
}
